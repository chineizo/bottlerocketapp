package com.example.bottlerocketapp

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.bottlerocketapp.pojo.Store
import com.example.bottlerocketdemo.R
import com.squareup.picasso.Picasso

class StoreAdapter(private val context: Context, private var stores: List<Store>) :
    RecyclerView.Adapter<StoreViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        StoreViewHolder(LayoutInflater.from(context).inflate(R.layout.store_item, parent, false))


    override fun getItemCount() = stores.size

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        val store = stores[position]
        holder.bindItems(store)
        Picasso.with(context).load(store.getStoreLogoURL()).resize(60, 60).into(holder.logo)
    }

    fun update (stores: List<Store>) {
        this.stores = stores
        notifyDataSetChanged()
    }

    fun getItems () = stores

    fun getItem (position: Int) = stores [position]

}

class StoreViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val logo = itemView.findViewById<ImageView>(R.id.logoView)
    fun bindItems(store: Store) {
        val address = itemView.findViewById<TextView>(R.id.address)
        address.text = store.getAddress()

        val phoneNumber = itemView.findViewById<TextView>(R.id.phoneNumber)
        phoneNumber.text = store.getPhone()

    }

}