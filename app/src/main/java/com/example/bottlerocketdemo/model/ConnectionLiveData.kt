package com.example.bottlerocketapp.model

import android.arch.lifecycle.LiveData
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo

/**
 * Class the Monitor Data Connection Status (WIFI or LTE)
 */
class ConnectionLiveData(context: Context) : LiveData<ConnectionModel>() {
    private val context: Context = context

    override fun onInactive() {
        super.onInactive()
        context.unregisterReceiver(networkReceiver)
    }

    override fun onActive() {
        super.onActive()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(networkReceiver, filter)
    }

    private var networkReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            intent.extras?.let {
                var activeNetwork = intent.extras.get(ConnectivityManager.EXTRA_NETWORK_INFO) as NetworkInfo
                var isConnected = activeNetwork != null && activeNetwork.isConnected
                if (isConnected) {
                    when (activeNetwork.type) {
                        ConnectivityManager.TYPE_WIFI -> {
                            postValue(ConnectionModel(ConnectivityManager.TYPE_WIFI, true))
                        }
                        ConnectivityManager.TYPE_MOBILE -> {
                            postValue(ConnectionModel(ConnectivityManager.TYPE_MOBILE, true))
                        }
                    }
                } else {
                    postValue(ConnectionModel(0, false))
                }
            }
        }

    }
}