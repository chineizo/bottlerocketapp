package com.example.bottlerocketapp.model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.example.bottlerocketapp.pojo.Store
import com.example.bottlerocketapp.database.StoreDatabase

/**
 * View Model Class for Inserting and Retrieving Store data
 */
class StoreViewDBModel(context: Application) : AndroidViewModel(context) {
    private val dao = StoreDatabase.getInstance(context)?.storeDao()
    var storeMutableLiveData: MutableLiveData<List<Store>> = getStores()

    /**
     * Insert a Store in the Room Database
     */
    fun insert(store: Store) {
        dao?.insertStore(store)
    }

    /**
     * Insert Stores in the Room Database
     */
    fun insert(stores: List<Store>) {
        dao?.insertStores(stores)
    }

    /**
     * Retrieve Stores from the Room Database
     */
    private fun getStores(): MutableLiveData<List<Store>> {
        var data: MutableLiveData<List<Store>> = MutableLiveData()
        dao?.let {
            data.value = it.getStores()
        }
        return data
    }
}