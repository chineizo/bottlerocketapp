package com.example.bottlerocketapp.model

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.example.bottlerocketapp.pojo.Store
import com.example.bottlerocketapp.pojo.StoreData
import com.example.bottlerocketapp.repository.StoreApiController
import com.example.bottlerocketapp.repository.StoreRepository
import com.google.gson.Gson

class StoreViewNetworkModel (context: Application): AndroidViewModel (context){
    private val TAG = StoreViewNetworkModel::class.java.simpleName
    var storeMutableLiveData: MutableLiveData<List<Store>> = MutableLiveData()
    private val storeRepository = StoreRepository.getInstance()

    /**
     * Retrieve Stores from the Network API
     */
    fun getStores () {
        storeRepository?.let {
            StoreApiController (storeRepository).getStore {response->
                Log.d(TAG, "Response returned:$response")
                var parser = Gson ()
                var storeData: StoreData = parser.fromJson(response.toString(), StoreData::class.java)
                Log.d(TAG, "StoreData:$storeData")
                storeData.getStores()?.let {
                    storeMutableLiveData.value = it
                }
            }
        }
    }
}