package com.example.bottlerocketapp.model

/**
 * Class that contains the connection state of storeMutableLiveData connectivity
 */
data class ConnectionModel (var type: Int, var isConnected: Boolean)
