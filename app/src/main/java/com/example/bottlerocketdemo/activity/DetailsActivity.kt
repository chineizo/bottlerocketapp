package com.example.bottlerocketdemo.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.bottlerocketapp.Constants

import com.example.bottlerocketapp.pojo.Store
import com.example.bottlerocketdemo.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details.*


class DetailsActivity : AppCompatActivity(){
    private val TAG = DetailsActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        title = getString(R.string.store_detail)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }

        val store = intent.getSerializableExtra(Constants.INTENT_PARAM) as? Store
        store?.let {
            populateView(it)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    /**
     * Render the Store storeMutableLiveData to UI components
     */
    private fun populateView(store: Store) {
        storeId.text = store.getStoreID()
        storeName.text = store.getName()
        phoneNumber.text = store.getPhone()
        address.text = store.getAddress()
        city.text = store.getCity()
        state.text = store.getState()
        latitude.text = store.getLatitude()
        longitude.text = store.getLongitude()
        Picasso.with(this).load(store.getStoreLogoURL())
            .resize(300, 180).into(logoView)
    }

}