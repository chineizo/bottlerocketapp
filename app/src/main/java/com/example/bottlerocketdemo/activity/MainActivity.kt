package com.example.bottlerocketdemo.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import com.example.bottlerocketapp.Constants
import com.example.bottlerocketapp.StoreAdapter
import com.example.bottlerocketapp.database.StoreDatabase
import com.example.bottlerocketapp.model.ConnectionLiveData
import com.example.bottlerocketapp.model.StoreViewDBModel
import com.example.bottlerocketapp.model.StoreViewNetworkModel
import com.example.bottlerocketapp.pojo.Store
import com.example.bottlerocketdemo.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.simpleName
    private var storeDb: StoreDatabase? = null

    interface ClickListener {
        fun onClick(view: View, position: Int)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        storeDb = StoreDatabase.getInstance(this)

        storeView.layoutManager = LinearLayoutManager(this)
        storeView.adapter = StoreAdapter(this, ArrayList())
        storeView.addOnItemTouchListener(RecyclerTouchListener(this, object : ClickListener {
            override fun onClick(view: View, position: Int) {
                val item = (storeView.adapter as StoreAdapter).getItem(position)
                val intent = Intent(this@MainActivity, DetailsActivity::class.java)
                intent.putExtra(Constants.INTENT_PARAM, item)
                startActivity(intent)
            }
        }))

        var storeDatabaseViewModel = ViewModelProviders.of(this).get(StoreViewDBModel::class.java)
        var storeNetworkViewModel = ViewModelProviders.of(this).get(StoreViewNetworkModel::class.java)

        //Observe when Store data is retrieved from the Network
        storeNetworkViewModel.storeMutableLiveData.observe(this, Observer<List<Store>> { stores ->
            Log.d(TAG, "Length of Stores from Network API call is:" + stores?.size)
            stores?.let {
                storeDatabaseViewModel.insert(it)
                (storeView.adapter as StoreAdapter).update(it)
            }
        })


        //Observe when Store data is retrieved from the Room Database
        storeDatabaseViewModel.storeMutableLiveData.observe(this, Observer<List<Store>> { stores ->
            Log.d(TAG, "Length of Stores from Room DB is:" + stores?.size)

            //No Stores stored in the database, then fetch from network
            if (stores?.size == 0) {
                val snack = Snackbar.make(
                    findViewById(R.id.content),
                    getString(R.string.no_data_in_database),
                    Snackbar.LENGTH_LONG
                )
                snack.show()
                storeNetworkViewModel.getStores()
            } else {
                //Use Stores stored in database to populate the recyclerview
                stores?.let {
                    (storeView.adapter as StoreAdapter).update(it)
                }
            }
        })


        //Listen for Network Connectivity and inform user of disconnecion
        var connectionLiveData = ConnectionLiveData(this.applicationContext)
        connectionLiveData.observe(this, Observer {
            it?.let { connectionModel ->
                Log.d(TAG, "Is Data Connected? " + connectionModel.isConnected)
                if (!connectionModel.isConnected) {
                    val snack = Snackbar.make(
                        findViewById(R.id.content),
                        getString(R.string.data_connection_lost),
                        Snackbar.LENGTH_LONG
                    )
                    snack.show()
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        storeDb?.destroyInstance() //clean up Room Database resource
    }

    /**
     * Listener for touch feedback on the RecyclerView
     */
    internal class RecyclerTouchListener(
        context: Context,
        private val clickListener: ClickListener
    ) : RecyclerView.OnItemTouchListener {
        private val TAG = RecyclerTouchListener::class.java.simpleName
        private val gestureDetector: GestureDetector =
            GestureDetector(context, object : GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapUp(e: MotionEvent?): Boolean {
                    return true
                }
            })

        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {
            Log.d(TAG, "OnTouch Event occurred")
        }

        override fun onInterceptTouchEvent(recyclerView: RecyclerView, event: MotionEvent): Boolean {
            val child = recyclerView.findChildViewUnder(event.x, event.y)
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(event)) {
                clickListener.onClick(child, recyclerView.getChildPosition(child))
            }
            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {
            Log.d(TAG, "on Request Disallow Intercept called")
        }
    }
}
