package com.example.bottlerocketapp

import android.app.Application
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class MainApplication : Application() {
    private val TAG = MainApplication::class.java.simpleName

    companion object {
        @get:Synchronized
        var instance: MainApplication? = null
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    /**
     * Create Instance of RequestQueue object
     */
    private val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(applicationContext)
            }
            return field
        }

    /**
     * Add request to queue and tag it
     */
    fun <T> addToRequestQueue(request: Request<T>, tag: String) {
        request.tag = if (tag.isEmpty()) TAG else tag
        requestQueue?.add(request)
    }
}