package com.example.bottlerocketapp.repository

import org.json.JSONObject

interface StoreAPI {
    fun getStore(completionHandler: (response: JSONObject?) -> Unit)
}