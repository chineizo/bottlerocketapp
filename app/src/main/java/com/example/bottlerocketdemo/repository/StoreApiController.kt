package com.example.bottlerocketapp.repository

import org.json.JSONObject

class StoreApiController constructor (storeApi: StoreAPI) : StoreAPI {
    private val service: StoreAPI = storeApi
    override fun getStore(completionHandler: (response: JSONObject?) -> Unit) {
        service.getStore(completionHandler)
    }
}