package com.example.bottlerocketapp.repository

import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.bottlerocketapp.MainApplication
import org.json.JSONObject


class StoreRepository : StoreAPI {
    private val TAG = StoreRepository::class.java.simpleName
    private val URL = "http://sandbox.bottlerocketapps.com/BR_Android_CodingExam_2015_Server/stores.json"

    companion object {
        private var INSTANCE: StoreRepository? = null
        fun getInstance(): StoreRepository? {
            if (INSTANCE == null) {
                synchronized(StoreRepository::class) {
                    INSTANCE = StoreRepository()
                }
            }
            return INSTANCE
        }
    }


    override fun getStore(completionHandler: (response: JSONObject?) -> Unit) {
        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, URL, null,
            Response.Listener { response ->
                Log.d(TAG, "Response: $response")
                completionHandler(response)
            }, Response.ErrorListener {
                completionHandler(null)
                Log.e(TAG, "Error occurred sending StoreEntity Request: ", it)

            })
        // Access the RequestQueue through your singleton class.
        MainApplication.instance?.let {
            it.addToRequestQueue(jsonObjectRequest, TAG)
        }
    }


}