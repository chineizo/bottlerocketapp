package com.example.bottlerocketapp.pojo

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StoreData {
    @SerializedName("stores")
    @Expose
    private var stores: List<Store>? = null

    fun getStores(): List<Store>? {
        return stores
    }

    fun setStores(stores: List<Store>) {
        this.stores = stores

        
    }
}