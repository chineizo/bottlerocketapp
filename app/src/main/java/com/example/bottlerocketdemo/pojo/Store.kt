package com.example.bottlerocketapp.pojo

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity (tableName = "store")
class Store : Serializable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @ColumnInfo(name = "address")
    @SerializedName("address")
    @Expose
    private var address: String? = null

    @ColumnInfo(name = "city")
    @SerializedName("city")
    @Expose
    private var city: String? = null

    @ColumnInfo(name = "name")
    @SerializedName("name")
    @Expose
    private var name: String? = null

    @ColumnInfo(name = "latitude")
    @SerializedName("latitude")
    @Expose
    private var latitude: String? = null

    @ColumnInfo(name = "zipcode")
    @SerializedName("zipcode")
    @Expose
    private var zipcode: String? = null

    @ColumnInfo(name = "storeLogoURL")
    @SerializedName("storeLogoURL")
    @Expose
    private var storeLogoURL: String? = null

    @ColumnInfo(name = "phone")
    @SerializedName("phone")
    @Expose
    private var phone: String? = null

    @ColumnInfo(name = "longitude")
    @SerializedName("longitude")
    @Expose
    private var longitude: String? = null

    @ColumnInfo(name = "storeID")
    @SerializedName("storeID")
    @Expose
    private var storeID: String? = null

    @ColumnInfo(name = "state")
    @SerializedName("state")
    @Expose
    private var state: String? = null

    fun getAddress(): String? {
        return address
    }

    fun setAddress(address: String) {
        this.address = address
    }

    fun getCity(): String? {
        return city
    }

    fun setCity(city: String) {
        this.city = city
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getLatitude(): String? {
        return latitude
    }

    fun setLatitude(latitude: String) {
        this.latitude = latitude
    }

    fun getZipcode(): String? {
        return zipcode
    }

    fun setZipcode(zipcode: String) {
        this.zipcode = zipcode
    }

    fun getStoreLogoURL(): String? {
        return storeLogoURL
    }

    fun setStoreLogoURL(storeLogoURL: String) {
        this.storeLogoURL = storeLogoURL
    }

    fun getPhone(): String? {
        return phone
    }

    fun setPhone(phone: String) {
        this.phone = phone
    }

    fun getLongitude(): String? {
        return longitude
    }

    fun setLongitude(longitude: String) {
        this.longitude = longitude
    }

    fun getStoreID(): String? {
        return storeID
    }

    fun setStoreID(storeID: String) {
        this.storeID = storeID
    }

    fun getState(): String? {
        return state
    }

    fun setState(state: String) {
        this.state = state
    }
}