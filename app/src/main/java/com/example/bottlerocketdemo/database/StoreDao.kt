package com.example.bottlerocketapp.database

import android.arch.persistence.room.*
import com.example.bottlerocketapp.pojo.Store

@Dao
interface StoreDao {
    @Query("SELECT * FROM Store")
    fun getStores (): List<Store>

    @Insert
    fun insertStores (storeEntities: List<Store>)

    @Insert
    fun insertStore (storeEntity: Store)

    @Update
    fun updateStore (storeEntity: Store)

    @Delete
    fun deleteStore (storeEntity: Store)
}