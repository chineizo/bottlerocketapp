package com.example.bottlerocketapp.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.bottlerocketapp.pojo.Store

@Database (entities = [Store::class], version = 1)
abstract class StoreDatabase : RoomDatabase (){
    abstract fun storeDao (): StoreDao

    companion object {
        private var INSTANCE: StoreDatabase? = null

        fun getInstance (context: Context): StoreDatabase? {
            if (INSTANCE == null) {
                synchronized(StoreDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, StoreDatabase::class.java, "store.db")
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }
    }

    fun destroyInstance () {
        INSTANCE = null
    }
}